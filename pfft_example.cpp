//
// Created by me on 14/10/24.
//

#include "pfft_example.h"
#include <iostream>
#include <mpi.h>
#include <fftw3-mpi.h>  // Includes the FFTW and PFFT extensions for MPI

int main(int argc, char **argv) {
    // Initialize MPI
    MPI_Init(&argc, &argv);

    // Get the number of processes and the rank of this process
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // Initialize FFTW for MPI
    fftw_mpi_init();

    // Define the 3D grid size
    const ptrdiff_t N0 = 128, N1 = 128, N2 = 128;  // Size of the 3D grid

    // Define local sizes for parallel decomposition
    ptrdiff_t local_n0, local_0_start, local_n1, local_1_start;

    // Get the local data sizes and offsets
    ptrdiff_t alloc_local = fftw_mpi_local_size_3d(N0, N1, N2, MPI_COMM_WORLD, &local_n0, &local_0_start);

    // Allocate memory for the input and output arrays
    fftw_complex *data = fftw_alloc_complex(alloc_local);

    // Plan for a forward FFT
    fftw_plan plan = fftw_mpi_plan_dft_3d(N0, N1, N2, data, data, MPI_COMM_WORLD, FFTW_FORWARD, FFTW_MEASURE);

    // Initialize input data (just an example, could be any data)
    for (ptrdiff_t i = 0; i < local_n0; ++i) {
        for (ptrdiff_t j = 0; j < N1; ++j) {
            for (ptrdiff_t k = 0; k < N2; ++k) {
                ptrdiff_t index = (i * N1 * N2) + (j * N2) + k;
                data[index][0] = rank + i + j + k;  // Real part
                data[index][1] = 0.0;               // Imaginary part
            }
        }
    }

    // Perform the forward FFT
    fftw_execute(plan);

    // Example: Process data (printing the transformed data from the first rank only)
    if (rank == 0) {
        std::cout << "FFT result (rank 0): " << std::endl;
        for (ptrdiff_t i = 0; i < local_n0; ++i) {
            for (ptrdiff_t j = 0; j < N1; ++j) {
                for (ptrdiff_t k = 0; k < N2; ++k) {
                    ptrdiff_t index = (i * N1 * N2) + (j * N2) + k;
                    std::cout << "(" << data[index][0] << ", " << data[index][1] << ")" << std::endl;
                }
            }
        }
    }

    // Clean up
    fftw_destroy_plan(plan);
    fftw_free(data);
    fftw_mpi_cleanup();

    // Finalize MPI
    MPI_Finalize();

    return 0;
}

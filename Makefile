all:
	gcc -c test_sched.c -o test_sched.o
	gcc -I/usr/lib/x86_64-linux-gnu/openmpi/include -I/usr/lib/mpich/include -I../tests -c fftw_mpi_test.c -o fftw_mpi_test.o
	gcc -I/usr/lib/x86_64-linux-gnu/openmpi/include -I/usr/lib/mpich/include -I../tests -c rfftw_mpi_test.c -o rfftw_mpi_test.o
	gcc -I/usr/lib/x86_64-linux-gnu/openmpi/include -I/usr/lib/mpich/include -c test_transpose_mpi.c -o test_transpose_mpi.o
#	gcc -lfftw_mpi test_sched.o -o test_sched
	gcc -lfftw_mpi fftw_mpi_test.o ../tests/test_main.o -o fftw_mpi_test
	gcc -lrfftw_mpi rfftw_mpi_test.o ../tests/test_main.o -o rfftw_mpi_test
	gcc -lfftw_mpi test_transpose_mpi.o -o test_transpose_mpi

clean:
	rm *.o test_sched fftw_mpi_test rfftw_mpi_test test_transpose_mpi

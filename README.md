# fichier pfft_example.cpp

fichier fourni par chatgpt, en réponse à la question:
"give me example of pfft usage in C++ code"

## pour le compiler
```commandline
mpic++ -o pfft_example pfft_example.cpp -lfftw3_mpi -lfftw3 -lm
```

## Exécuter
```commandline
mpirun -np 4 ./pfft_example
```

## Tunings
Performance Tuning: PFFT supports different optimization flags like FFTW_MEASURE for planning. You can also use FFTW_PATIENT or FFTW_ESTIMATE depending on your performance vs. planning time requirements.
cmake_minimum_required(VERSION 3.22)
project(pfft_example CXX C)

set(CMAKE_CXX_STANDARD 17)

# au cas où on utilise ninja au lieu de makefiles
set(CMAKE_VERBOSE_MAKEFILE ON CACHE BOOL "ON")

set(CMAKE_CXX_FLAGS_DEBUG "-g -DDEBUG -std=c++20")
set(CMAKE_C_FLAGS_DEBUG "-g -DDEBUG")

find_package(MPI REQUIRED)
message(STATUS "Run: ${MPIEXEC} ${MPIEXEC_NUMPROC_FLAG} ${MPIEXEC_MAX_NUMPROCS} ${MPIEXEC_PREFLAGS} EXECUTABLE ${MPIEXEC_POSTFLAGS} ARGS")

# attention cherche le build hdf5 dans ${HOME}/projets
#set(HDF5VERSION 1.14.4.3)
set(HDF5VERSION 1.12.3)
#set(dir1 $ENV{HOME}/LIBRARY_PARA/LIBRARIES/ompi500/hdf5-${HDF5VERSION}/)
set(dir1 $ENV{HOME}/LIBRARY_PARA/LIBRARIES/hdf5-${HDF5VERSION}/)
message("avant dir1 : " ${dir1})
IF (EXISTS "${dir1}" AND IS_DIRECTORY "${dir1}")
    message("dir1 : " ${dir1})
    if (APPLE AND UNIX)
        set(HDF5_DIR ${dir1}/Darwin/TGZ/HDF5-${HDF5VERSION}-Darwin/HDF_Group/HDF5/${HDF5VERSION}/share/cmake/hdf5/)
    else ()
        if (UNIX)
            set(HDF5_DIR ${dir1}/Linux/TGZ/HDF5-${HDF5VERSION}-Linux/HDF_Group/${HDF5VERSION}/share/cmake/hdf5/)
        endif ()
    endif ()
endif ()
set(FIND_HDF_COMPONENTS C CXX HL HLCXX static)
find_package(HDF5 NAMES "hdf5" COMPONENTS ${FIND_HDF_COMPONENTS})
find_package (HDF5)
INCLUDE_DIRECTORIES(${HDF5_INCLUDE_DIR})
message("HDF5_DIR : " ${HDF5_DIR})
find_package(HDF5 1.12.3)
message("HDF5_IS_PARALLEL: ${HDF5_IS_PARALLEL}")

include_directories(${HDF5_INCLUDE_DIRS} ${FFTW_INCLUDE_DIR})
add_executable(pfft_example
        pfft_example.cpp
        pfft_example.h
)
target_link_libraries(pfft_example PRIVATE
        ${HDF5_LIBRARIES}
        MPI::MPI_C MPI::MPI_CXX
        -L${FFTW_INCLUDE_DIR}/../lib fftw3_mpi fftw3
)

